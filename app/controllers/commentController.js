//import thư viện mongoose 
const mongoose = require("mongoose");
//import comment model
const commentModel = require("../models/commentModel");

//function create comment
const createComment = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let body = request.body;

    //B2: validate dữ liệu
    if(!body.name ){
        return response.status(400).json({
            status:"Bad request",
            message:"Name is not valid"
        })
    } 
    if(!body.email ){
        return response.status(400).json({
            status:"Bad request",
            message:"Email is not valid"
        })
    }
    if(!body.body){
        return response.status(400).json({
            status:"Bad request",
            message:"Body is not valid"
        })
    }

    //B3: thao tác với CSDL
    let newComment = {
        _id: mongoose.Types.ObjectId(),
        postId : mongoose.Types.ObjectId(),
        name: body.name,
        email: body.email,
        body: body.body
    }
    if(body.name !== undefined){
        newComment.name = body.name
    }
    if(body.email !== undefined){
        newComment.email = body.email
    }
    if(body.body !== undefined){
        newComment.body = body.body
    }
    commentModel.create(newComment, (error,data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:"Create new comment successfully",
            data: data
        })
    })
}
//function get all comment
const getAllComment = (request, response) => {
    commentModel.find((error,data) =>{
        if(error) {
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:" Get all comments successfully",
            data: data
        })
    })
}
//function update comment by id
const updateComment = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let commentId = request.params.commentId;
    let body = request.body

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return response.status(400).json({
            status:"Bad request",
            message:" Comment id is not valid"
        })
    }
    if(!body.name ){
        return response.status(400).json({
            status:"Bad request",
            message:"Name is not valid"
        })
    } 
    if(!body.email ){
        return response.status(400).json({
            status:"Bad request",
            message:"Email is not valid"
        })
    }
    if(!body.body){
        return response.status(400).json({
            status:"Bad request",
            message:"Body is not valid"
        })
    }
    //B3: gọi model thao tác CSDL
    const commentUpdate = {};
    if(body.name !== undefined){
        commentUpdate.name = body.name
    }
    if(body.email !== undefined){
        commentUpdate.email = body.email
    }
    if(body.body !== undefined){
        commentUpdate.body = body.body
    }
    commentModel.findByIdAndUpdate(commentId, commentUpdate, (error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status:`Update comment with id${commentId} successfully`,
            data: data
        })
    })
}
//function get comment by id
const getCommentById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    let commentId = request.params.commentId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId)){
        return response.status(400).json({
            status:"Bad request",
            message:"Comment id is not valid"
        })
    }
    //B3: gọi model chứa id thao tác với csdl
    commentModel.findById(commentId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Get comment by id ${commentId} successfully`,
            data: data
        })
    })
}
//function delete comment by id
const deleteComment = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    let commentId = request.params.commentId;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(commentId )){
        return response.status(400).json({
            status:"Bad request",
            message: "Comment id is not valid"
        })
    }
    //B3: thao tác với CSDL
    commentModel.findByIdAndRemove(commentId,(error, data) =>{
        if(error){
            return response.status(500).json({
                status:"Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status:`Deleted comment by id ${commentId} successfully`
        })
    })
}
module.exports = {
    createComment,
    getAllComment,
    updateComment,
    getCommentById,
    deleteComment

}