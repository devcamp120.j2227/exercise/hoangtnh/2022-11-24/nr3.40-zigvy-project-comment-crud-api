//import express js
const express = require ("express");

//import comment controller
const commentController = require("../controllers/commentController");

const router = express.Router();

router.post("/comments", commentController.createComment);
router.get("/comments", commentController.getAllComment);
router.put("/comments/:commentId", commentController.updateComment);
router.get("/comments/:commentId", commentController.getCommentById);
router.delete("/comments/:commentId", commentController.deleteComment);
module.exports = router;